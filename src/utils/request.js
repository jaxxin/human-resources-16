// 导出一个axios的实例  而且这个实例要有请求拦截器 响应拦截器
import store from '@/store'
import axios from 'axios'
import router from '@/router'
import { Message } from 'element-ui'
import { getTimeStamp } from '@/utils/auth'
import { Promise } from 'core-js'
const TimeOut = 3600 // 定义超时时间 /s
// import store from '@/store'
// 创建一个axios的实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 6000
})

// 是否超时
// 超时逻辑  (当前时间  - 缓存中的时间) 是否大于 时间差
function IsCheckTimeOut() {
  var currentTime = Date.now() // 当前时间戳
  var timeStamp = getTimeStamp() // 缓存时间戳
  return (currentTime - timeStamp) / 1000 > TimeOut
}

// 请求拦截器 [注入 token]
service.interceptors.request.use(
  config => {
    // 在这个位置需要统一的去注入token
    if (store.getters.token) {
      // 只有在有token的情况下 才有必要去检查时间戳是否超时
      if (IsCheckTimeOut()) {
        //  true  token 过期
        store.dispatch('user/logout')
        router.push('/login')
        return Promise.reject(new Error('token超时了'))
      }
      // 如果token存在 注入token
      config.headers['Authorization'] = `Bearer ${store.getters.token}`
    }
    return config // 必须返回配置
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器  [集中的处理错误，数据处理]
service.interceptors.response.use(
  response => {
    //  后端返回数据  respons.data
    const { data, success, message } = response.data
    if (success) {
      return data
    } else {
      // 业务错误
      Message.error(message)
      return Promise.reject(new Error(message))
    }
  },
  // 请求错误
  error => {
    if (
      error.response &&
      error.response.data &&
      error.response.data.code === 10002
    ) {
      //  code = 10002 时，后台表示token 过期
      // 登出action ，删除token
      store.dispatch('user/login')
      router.push('/login')
      // return Promise.reject(error)
    } else {
      Message.error(error.message)
      // return Promise.reject(error)
    }
    return Promise.reject(error)
  }
)

// 导出axios实例  暴漏出去
export default service
