// 封装了 对token 的持久化操作
// js-cookie 是对 cookie 操作的第三方模块
import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

// 设置一个独一无二的key
const timeKey = 'hrsaas-timestamp-key'
// 获取时间戳
export function getTimeStamp() {
  return Cookies.get(timeKey)
}
// 设置时间戳
export function serTimeStamp() {
  return Cookies.set(timeKey, Date.now())
}
