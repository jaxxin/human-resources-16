import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login', // 所有的路径都要跨域，表示所有的接口都要带 /api
    method: 'POST',
    data
  })
}

// 获取用户基本资料
export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 获取用户基本信息，【暂时为了获取用户图片】
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

export function getInfo(token) {}

export function logout() {}
