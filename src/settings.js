// 系统的一些配置

module.exports = {
// 系统标题
  title: '人力资源管理平台',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 是否固定布局界面的头部
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // s是否显示侧边栏的logo
  sidebarLogo: true
}
