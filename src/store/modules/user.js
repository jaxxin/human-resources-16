import { getToken, setToken, removeToken, serTimeStamp } from '@/utils/auth'
import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { resetRouter } from '@/router'

// 状态
const state = {
  // 设置 token 的初始状态 token持久化 =》 放到缓存中
  token: getToken(),
  userInfo: {}
}

// 修改状态
const mutations = {
  // 设置 token
  setToken(state, token) {
    // 设置 token
    state.token = token
    // c持久化
    setToken(token)
  },
  removeToken(state) {
    state.token = null
    removeToken()
  },
  // 设置用户信息
  setUserInfo(state, userInfo) {
    state.userInfo = { ...userInfo }
  },
  // 删除用户信息
  removeUserInfo(state) {
    state.userInfo = {}
  }

}

// 执行异步
const actions = {
  // 定义login action  也需要参数 调用action时 传递过来的参数
  async login(context, data) {
    const result = await login(data) // 实际上就是一个promise  result就是执行的结果
    // axios默认给数据加了一层data

    // 表示登录接口调用成功 也就是意味着你的用户名和密码是正确的
    // 现在有用户token
    // actions 修改state 必须通过mutations
    context.commit('setToken', result)
    // // 写入时间戳
    serTimeStamp()
  },
  // 退出  登出的action
  logout(context) {
    // 删除token
    context.commit('removeToken')
    // 删除用户资料
    context.commit('removeUserInfo')
    // 重置路由
    resetRouter()
    // 重置菜单
    // 还有一步  vuex中的数据是不是还在
    // 要清空permission模块下的state数据
    // vuex中 user子模块  permission子模块
    // 子模块调用子模块的action  默认情况下 子模块的context是子模块的
    // 父模块 调用 子模块的action
    context.commit('permission/setRoutes', [], { root: true })
    // 子模块调用子模块的action 可以 将 commit的第三个参数 设置成  { root: true } 就表示当前的context不是子模块了 而是父模块
  },
  // 获取用户资料
  async getUserInfo(context) {
    const result = await getUserInfo()
    // 此时已经获取了用户资料，然后获取头像
    const baseInfo = await getUserDetailById(result.userId)
    // 将两个结果合并
    context.commit('setUserInfo', { ...result, ...baseInfo })
    return result
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
