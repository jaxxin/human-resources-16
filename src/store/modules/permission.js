import { constantRoutes, asyncRoutes } from '@/router'
const state = {
  // 完整的用户路由映射表，但是初始值只能说静态的
  routes: constantRoutes
}
const mutations = {
  // 修改 routes 的信息
  setRoutes(state, newRoutes) {
    // newRoutes 就是筛选出来的该用户的动态路由
    // 下面这样写不对，是业务不对
    // state.routes = [...state.routes, ...newRoutes]
    // 应该是每次更新  都在静态路由的基础上进行追加
    state.routes = [...constantRoutes, ...newRoutes]
  }
}
const actions = {
  // 筛选权限路由
  filterRoutes(context, menus) {
    const routes = []
    menus.forEach(key => {
      // 得到一个数组，有可能有元素，也可能是空数组
      routes.push(...asyncRoutes.filter(item => item.name === key))
    })
    //  将动态路由提交给mutations
    context.commit('setRoutes', routes)
    // 给 路由addRoutes
    return routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
